import logging
import os
from base64 import urlsafe_b64encode
from datetime import datetime
from importlib import import_module, reload
from unittest.mock import Mock
from uuid import uuid4

import pytest
import ujson
from gnar_gear import GnarApp
from pystache import render

from piste.app.user.constants import (
    EMAIL_RESET_PASSWORD_BODY_TEXT,
    EMAIL_RESET_PASSWORD_PARAMETERS,
    EMAIL_RESET_PASSWORD_SUBJECT,
    EMAIL_SIGNUP_BODY_TEXT,
    EMAIL_SIGNUP_PARAMETERS,
    EMAIL_SIGNUP_SUBJECT,
    ERROR_ACCOUNT_LOCKED,
    ERROR_ACTION_ID_EMAIL_AND_NEW_PASSWORD_ARE_REQUIRED,
    ERROR_USER_ALREADY_EXISTS,
    ERROR_USER_DOES_NOT_EXIST,
    ERROR_USER_HAS_ALREADY_BEEN_ACTIVATED,
    ERROR_USER_HAS_NOT_BEEN_ACTIVATED,
    ERROR_EMAIL_AND_INVITE_ID_ARE_REQUIRED,
    ERROR_EMAIL_IS_REQUIRED,
    ERROR_INVALID_EMAIL_OR_PASSWORD,
    ERROR_INVALID_INVITE_ID,
    ERROR_INVALID_PASSWORD_RESET_ID,
    ERROR_INVITE_HAS_EXPIRED,
    ERROR_NO_BOTS_ALLOWED,
    ERROR_PASSWORD_IS_REQUIRED,
    ERROR_PASSWORD_PWNED,
    ERROR_PASSWORD_RESET_LINK_HAS_EXPIRED,
    ERROR_PASSWORD_NO_SPACES,
    ERROR_PASSWORD_TOO_SHORT,
    ERROR_PASSWORD_TOO_WEAK
)
from piste.test.constants import ENVIRONMENT

log = logging.getLogger()


class MockResponse:

    def __init__(self, data):
        self.text = data

    def json(self):
        return self.text


class MockBoto3:

    def __init__(self, mock):
        self.send_email = mock


class TestApis:

    app_gen = None
    default_response = {'status': 'ok'}
    email = 'mock@email.com'
    mock_data = {'firstName': 'Test', 'lastName': 'User', 'email': email}
    password_hash = '$argon2i$v=19$m=512,t=2,p=2$lEE9nyrB6J9PP49U4Ge5iw$mjgR0J7mULshxw0qGlsjMw'
    mock_data_with_password = {**mock_data, 'password': password_hash}
    requestsPostMock = Mock()
    postgresRunMock = Mock()

    @staticmethod
    def error_response(error):
        return {'error': error}

    @staticmethod
    def patch_db(monkeypatch, data):
        user_id = str(uuid4())
        record = type('record', (object,), {'id': user_id, 'data': data}) if data else None
        monkeypatch.setattr('postgres.Postgres.one', lambda *a, **kw: record)

    @staticmethod
    def post_data(user, path, data):
        return user.post(path, content_type='application/json', data=ujson.dumps(data))

    @staticmethod
    def put_data(user, path, data):
        return user.put(path, content_type='application/json', data=ujson.dumps(data))

    @staticmethod
    def whitelist_record(data):
        read_whitelist = \
            ['user_id', 'firstName', 'lastName', 'address1', 'address2', 'city', 'state', 'postalCode']
        return {k: v for k, v in data.items() if k in read_whitelist}

    @pytest.fixture
    def user(self, monkeypatch):
        monkeypatch.setattr('argparse.ArgumentParser.parse_args',
                            lambda *a, **kw: type('args', (object,), {'port': ''}))
        monkeypatch.setattr('flask.Flask.run', lambda *a, **kw: None)
        monkeypatch.setattr('os.environ', ENVIRONMENT)
        monkeypatch.setattr('postgres.Postgres.__init__', lambda *a, **kw: None)
        monkeypatch.setattr('requests.sessions.Session.request', self.requestsPostMock)
        monkeypatch.setattr('postgres.Postgres.run', self.postgresRunMock)
        mock_client = type('sqs_client', (object,), {'get_queue_url': lambda *a, **kw: {'QueueUrl': 'Canada'},
                                                     'receive_message': Mock(return_value={})})
        monkeypatch.setattr('boto3.Session.client', lambda *a, **kw: mock_client)
        monkeypatch.setattr('redis.StrictRedis',
                            lambda *a, **kw: type('redis_connection', (object,), {'get': lambda *a: None}))
        self.patch_db(monkeypatch, self.mock_data)
        app = GnarApp('piste', production=False, port=9400)
        app.run()
        yield app.flask.test_client()

    def test_user_activate(self, monkeypatch, user):
        invite_id = str(uuid4())
        response = self.post_data(user, '/user/activate', {'token': urlsafe_b64encode(b'/')})
        assert response.json == self.error_response(ERROR_EMAIL_AND_INVITE_ID_ARE_REQUIRED)
        token = urlsafe_b64encode('fake@faker.com/{}'.format(invite_id).encode('utf-8')).decode('utf-8')
        self.patch_db(monkeypatch, None)
        response = self.post_data(user, '/user/activate', {'token': token})
        assert response.json == self.error_response(ERROR_USER_DOES_NOT_EXIST)
        self.patch_db(monkeypatch, {**self.mock_data, 'password': 'Password!1'})
        response = self.post_data(user, '/user/activate', {'token': token})
        assert response.json == self.error_response(ERROR_USER_HAS_ALREADY_BEEN_ACTIVATED)
        self.patch_db(monkeypatch, self.mock_data)
        response = self.post_data(user, '/user/activate', {'token': token})
        assert response.json == self.error_response(ERROR_INVALID_INVITE_ID)
        self.patch_db(monkeypatch, {**self.mock_data, 'invite': {'id': invite_id, 'time': 0}})
        response = self.post_data(user, '/user/activate', {'token': token})
        assert response.json == self.error_response(ERROR_INVITE_HAS_EXPIRED)
        now = datetime.utcnow().timestamp()
        self.patch_db(monkeypatch, {**self.mock_data, 'invite': {'id': invite_id, 'time': now}})
        response = self.post_data(user, '/user/activate', {'token': token})
        assert set(response.json.keys()) == set(['user_id', 'access_token'])

    def test_user_get(self, monkeypatch, user):
        response = self.post_data(user, '/user/get', {})
        assert response.json == self.error_response('Missing Authorization Header')
        monkeypatch.setattr('flask_jwt_extended.view_decorators.verify_jwt_in_request', lambda: None)
        response = self.post_data(user, '/user/get', {})
        assert response.json == self.error_response(ERROR_EMAIL_IS_REQUIRED)
        self.patch_db(monkeypatch, None)
        response = self.post_data(user, '/user/get', {'email': self.email})
        assert response.json == {}
        self.patch_db(monkeypatch, self.mock_data)
        response = self.post_data(user, '/user/get', {'email': self.email})
        assert response.json == self.whitelist_record({'user_id': response.json['user_id'], **self.mock_data})

    def test_user_login(self, monkeypatch, user):
        monkeypatch.setattr('requests.sessions.Session.request', lambda *a, **kw: MockResponse({'success': False}))
        data = {'email': self.email, 'password': 'password'}
        response = self.post_data(user, '/user/login', data)
        assert response.json == self.error_response(ERROR_NO_BOTS_ALLOWED)
        monkeypatch.setattr('requests.sessions.Session.request', lambda *a, **kw: MockResponse({'success': True}))
        self.patch_db(monkeypatch, None)
        response = self.post_data(user, '/user/login', data)
        assert response.json == self.error_response(ERROR_INVALID_EMAIL_OR_PASSWORD)
        self.patch_db(monkeypatch, {'password': self.password_hash})
        response = self.post_data(user, '/user/login', {'email': self.email, 'password': 'bad password'})
        assert response.json == self.error_response(ERROR_INVALID_EMAIL_OR_PASSWORD)
        self.patch_db(monkeypatch, {'password': self.password_hash, 'locked': True})
        response = self.post_data(user, '/user/login', data)
        assert response.json == self.error_response(ERROR_ACCOUNT_LOCKED)
        record_data = {**self.mock_data, 'password': self.password_hash}
        self.patch_db(monkeypatch, record_data)
        response = self.post_data(user, '/user/login', data)
        assert response.json == self.whitelist_record({'user_id': response.json['user_id'], **record_data})
        assert 'password' not in response.json

    def test_user_reset_password(self, monkeypatch, user):
        response = self.post_data(user, '/user/reset-password', {})
        assert response.json == self.error_response(ERROR_ACTION_ID_EMAIL_AND_NEW_PASSWORD_ARE_REQUIRED)
        response = self.post_data(user, '/user/reset-password', {'email': self.email})
        assert response.json == self.error_response(ERROR_ACTION_ID_EMAIL_AND_NEW_PASSWORD_ARE_REQUIRED)
        data = {'email': self.email, 'actionId': str(uuid4())}
        response = self.post_data(user, '/user/reset-password', data)
        assert response.json == self.error_response(ERROR_ACTION_ID_EMAIL_AND_NEW_PASSWORD_ARE_REQUIRED)
        actionId = str(uuid4())
        data = {'email': self.email, 'actionId': actionId, 'password': 'NewPassword'}
        self.patch_db(monkeypatch, None)
        response = self.post_data(user, '/user/reset-password', data)
        assert response.json == self.error_response(ERROR_USER_DOES_NOT_EXIST)
        self.patch_db(monkeypatch, self.mock_data)
        response = self.post_data(user, '/user/reset-password', data)
        assert response.json == self.error_response(ERROR_USER_HAS_NOT_BEEN_ACTIVATED)
        self.patch_db(monkeypatch, {**self.mock_data, 'password': 'OldPassword'})
        response = self.post_data(user, '/user/reset-password', data)
        assert response.json == self.error_response(ERROR_INVALID_PASSWORD_RESET_ID)
        record = {**self.mock_data, 'password': 'OldPassword', 'passwordReset': {'id': actionId, 'time': 0}}
        self.patch_db(monkeypatch, record)
        response = self.post_data(user, '/user/reset-password', data)
        assert response.json == self.error_response(ERROR_PASSWORD_RESET_LINK_HAS_EXPIRED)
        now = datetime.utcnow().timestamp()
        record = {**self.mock_data, 'password': 'OldPassword', 'passwordReset': {'id': actionId, 'time': now}}
        self.patch_db(monkeypatch, record)
        response = self.post_data(user, '/user/reset-password', data)
        assert response.json == self.default_response

    def test_user_score_password(self, monkeypatch, user):
        response = self.post_data(user, '/user/score-password', {})
        assert response.json == self.error_response(ERROR_PASSWORD_IS_REQUIRED)
        response = self.post_data(user, '/user/score-password', {'password': 'Password12'})
        assert response.json == {
            'score': 1,
            'feedback': {
                'suggestions': [
                    'Add another word or two. Uncommon words are better.',
                    "Capitalization doesn't help very much."
                ],
                'warning': 'This is a very common password.'
            }
        }
        response = self.post_data(user, '/user/score-password', {'password': 'Password!x-k-c-d'})
        assert response.json == {'score': 4, 'feedback': {'suggestions': [], 'warning': ''}}

    def test_user_send_reset_password_email(self, monkeypatch, user):
        from piste.app.user.services import get_email_template_body_html
        origin = 'https://app.gnar.ca'
        EMAIL_TEMPLATE_BODY_HTML = get_email_template_body_html()
        response = self.post_data(user, '/user/send-reset-password-email', {})
        assert response.json == self.error_response(ERROR_EMAIL_IS_REQUIRED)
        monkeypatch.setattr('requests.sessions.Session.request', lambda *a, **kw: MockResponse({'success': False}))
        response = self.post_data(user, '/user/send-reset-password-email', {'email': self.email})
        assert response.json == self.error_response(ERROR_NO_BOTS_ALLOWED)
        monkeypatch.setattr('requests.sessions.Session.request', lambda *a, **kw: MockResponse({'success': True}))
        mockRequest = Mock()
        monkeypatch.setattr('postgres.Postgres.run', mockRequest)
        self.patch_db(monkeypatch, None)
        response = self.post_data(user, '/user/send-reset-password-email', {'email': self.email})
        sql, parameters = mockRequest.call_args[0]
        assert sql == 'insert into public."badPasswordResetRequests" (email, ip) values (%(email)s, %(ip)s)'
        assert parameters['email'] == self.email
        assert response.json == self.default_response
        mockRequest = Mock()
        monkeypatch.setattr('postgres.Postgres.run', mockRequest)
        mockSendEmail = Mock()
        monkeypatch.setattr('boto3.Session.client', lambda *a, **kw: MockBoto3(mockSendEmail))
        self.patch_db(monkeypatch, self.mock_data)
        response = self.post_data(user, '/user/send-reset-password-email', {'email': self.email})
        sql, params = mockRequest.call_args[0]
        assert sql == 'update public.user set data=%(data)s where id=%(id)s'
        action_id = ujson.loads(params['data'])['invite']['id']
        token = urlsafe_b64encode('{}/{}'.format(self.email, action_id).encode('utf-8')).decode('utf-8')
        email_params = {**EMAIL_SIGNUP_PARAMETERS,
                        'host': 'app.gnar.ca',
                        'origin': origin,
                        'project_name': 'Project Gnar',
                        'logo_url': 'https://s3-us-west-2.amazonaws.com/project-gnar/logo-125x125.png',
                        'token': token}
        _, params = mockSendEmail.call_args
        assert params['Destination'] == {'ToAddresses': [self.email]}
        assert params['Message']['Body']['Html']['Data'] == render(EMAIL_TEMPLATE_BODY_HTML, email_params)
        assert params['Message']['Body']['Text']['Data'] == EMAIL_SIGNUP_BODY_TEXT.format(origin, token)
        assert params['Message']['Subject']['Data'] == EMAIL_SIGNUP_SUBJECT
        self.patch_db(monkeypatch, {**self.mock_data, 'password': 'password'})
        response = self.post_data(user, '/user/send-reset-password-email', {'email': self.email})
        sql, params = mockRequest.call_args[0]
        assert sql == 'update public.user set data=%(data)s where id=%(id)s'
        action_id = ujson.loads(params['data'])['passwordReset']['id']
        token = urlsafe_b64encode('{}/{}'.format(self.email, action_id).encode('utf-8')).decode('utf-8')
        email_params = {**EMAIL_RESET_PASSWORD_PARAMETERS,
                        'host': 'app.gnar.ca',
                        'origin': origin,
                        'project_name': 'Project Gnar',
                        'logo_url': 'https://s3-us-west-2.amazonaws.com/project-gnar/logo-125x125.png',
                        'token': token}
        _, params = mockSendEmail.call_args
        assert params['Destination'] == {'ToAddresses': [self.email]}
        assert params['Message']['Body']['Html']['Data'] == render(EMAIL_TEMPLATE_BODY_HTML, email_params)
        assert params['Message']['Body']['Text']['Data'] == EMAIL_RESET_PASSWORD_BODY_TEXT.format(origin, token)
        assert params['Message']['Subject']['Data'] == EMAIL_RESET_PASSWORD_SUBJECT
        assert response.json == self.default_response

    def test_user_signup(self, monkeypatch, user):
        from piste.app.user.services import get_email_template_body_html
        origin = 'https://app.gnar.ca'
        EMAIL_TEMPLATE_BODY_HTML = get_email_template_body_html()
        response = self.post_data(user, '/user/signup', {})
        assert response.json == self.error_response(ERROR_EMAIL_IS_REQUIRED)
        monkeypatch.setattr('requests.sessions.Session.request', lambda *a, **kw: MockResponse({'success': False}))
        response = self.post_data(user, '/user/signup', {'email': self.email})
        assert response.json == self.error_response(ERROR_NO_BOTS_ALLOWED)
        monkeypatch.setattr('requests.sessions.Session.request', lambda *a, **kw: MockResponse({'success': True}))
        self.patch_db(monkeypatch, {**self.mock_data, 'password': 'password'})
        response = self.post_data(user, '/user/signup', {'email': self.email})
        assert response.json == self.error_response(ERROR_USER_ALREADY_EXISTS)
        mockRequest = Mock()
        monkeypatch.setattr('postgres.Postgres.run', mockRequest)
        mockSendEmail = Mock()
        monkeypatch.setattr('boto3.Session.client', lambda *a, **kw: MockBoto3(mockSendEmail))
        self.patch_db(monkeypatch, self.mock_data)
        response = self.post_data(user, '/user/signup', {'email': self.email})
        sql, params = mockRequest.call_args[0]
        assert sql == 'update public.user set data=%(data)s where id=%(id)s'
        action_id = ujson.loads(params['data'])['invite']['id']
        token = urlsafe_b64encode('{}/{}'.format(self.email, action_id).encode('utf-8')).decode('utf-8')
        email_params = {**EMAIL_SIGNUP_PARAMETERS,
                        'host': 'app.gnar.ca',
                        'origin': origin,
                        'project_name': 'Project Gnar',
                        'logo_url': 'https://s3-us-west-2.amazonaws.com/project-gnar/logo-125x125.png',
                        'token': token}
        _, params = mockSendEmail.call_args
        assert params['Destination'] == {'ToAddresses': [self.email]}
        assert params['Message']['Body']['Html']['Data'] == render(EMAIL_TEMPLATE_BODY_HTML, email_params)
        assert params['Message']['Body']['Text']['Data'] == EMAIL_SIGNUP_BODY_TEXT.format(origin, token)
        assert params['Message']['Subject']['Data'] == EMAIL_SIGNUP_SUBJECT
        mockRequest.reset_mock()
        mockSendEmail.reset_mock()
        self.patch_db(monkeypatch, None)
        response = self.post_data(user, '/user/signup', {'email': self.email})
        sql, params = mockRequest.call_args[0]
        assert sql == 'insert into public.user (id, data) values (%(id)s, %(data)s)'
        action_id = ujson.loads(params['data'])['invite']['id']
        token = urlsafe_b64encode('{}/{}'.format(self.email, action_id).encode('utf-8')).decode('utf-8')
        email_params = {**EMAIL_SIGNUP_PARAMETERS,
                        'host': 'app.gnar.ca',
                        'origin': origin,
                        'project_name': 'Project Gnar',
                        'logo_url': 'https://s3-us-west-2.amazonaws.com/project-gnar/logo-125x125.png',
                        'token': token}
        _, params = mockSendEmail.call_args
        assert params['Destination'] == {'ToAddresses': [self.email]}
        assert params['Message']['Body']['Html']['Data'] == render(EMAIL_TEMPLATE_BODY_HTML, email_params)
        assert params['Message']['Body']['Text']['Data'] == EMAIL_SIGNUP_BODY_TEXT.format(origin, token)
        assert params['Message']['Subject']['Data'] == EMAIL_SIGNUP_SUBJECT

    def test_user_update(self, monkeypatch, user):
        user_id = str(uuid4())
        path = '/user/{}'.format(user_id)
        response = user.put(path)
        assert response.json == self.error_response('Missing Authorization Header')
        monkeypatch.setattr('flask_jwt_extended.view_decorators.verify_jwt_in_request', lambda: None)
        monkeypatch.setattr('flask_jwt_extended.get_jwt_identity', Mock(return_value=self.email))
        services = import_module('piste.app.user.services')
        reload(services)
        data = {'firstName': 'Updated Test', 'lastName': 'User'}
        self.patch_db(monkeypatch, None)
        response = self.put_data(user, path, data)
        assert response.json == {}
        self.patch_db(monkeypatch, self.mock_data)
        response = self.put_data(user, path, data)
        updated_record = {'user_id': user_id, **self.mock_data, **data}
        assert response.json == self.whitelist_record(updated_record)
        data['password'] = 'badpass'
        response = self.put_data(user, path, data)
        assert response.json == self.error_response(ERROR_PASSWORD_TOO_SHORT)
        data['password'] = 'bad pass'
        response = self.put_data(user, path, data)
        assert response.json == self.error_response(ERROR_PASSWORD_NO_SPACES)
        data['password'] = 'Password!2'
        response = self.put_data(user, path, data)
        assert response.json == self.error_response(ERROR_PASSWORD_TOO_WEAK)
        monkeypatch.setattr('requests.sessions.Session.request', lambda *a, **kw: MockResponse(''))
        data['password'] = 'Password!x-k-c-d'
        response = self.put_data(user, path, data)
        assert response.json == self.whitelist_record(updated_record)
        data['password'] = 'gimnasto4ka2012'
        with open(os.path.join(os.path.dirname(__file__), 'mock_pwnedpasswords_response.txt')) as f:
            matches = f.read()
            monkeypatch.setattr('requests.sessions.Session.request', lambda *a, **kw: MockResponse(matches))
            response = self.put_data(user, path, data)
            assert response.json == self.error_response(ERROR_PASSWORD_PWNED)
