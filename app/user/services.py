import hashlib
import re
from base64 import urlsafe_b64decode, urlsafe_b64encode
from datetime import datetime, timedelta
from logging import getLogger
from os import path
from uuid import uuid4

from piste.app.user.constants import (
    DATA_READ_WHITELIST,
    DATA_UPDATE_WHITELIST,
    EMAIL_CHARSET,
    EMAIL_RESET_PASSWORD_BODY_TEXT,
    EMAIL_RESET_PASSWORD_PARAMETERS,
    EMAIL_RESET_PASSWORD_SUBJECT,
    EMAIL_SIGNUP_BODY_TEXT,
    EMAIL_SIGNUP_PARAMETERS,
    EMAIL_SIGNUP_SUBJECT,
    ERROR_ACCOUNT_LOCKED,
    ERROR_ACTION_ID_EMAIL_AND_NEW_PASSWORD_ARE_REQUIRED,
    ERROR_USER_ALREADY_EXISTS,
    ERROR_USER_DOES_NOT_EXIST,
    ERROR_USER_HAS_ALREADY_BEEN_ACTIVATED,
    ERROR_USER_HAS_NOT_BEEN_ACTIVATED,
    ERROR_EMAIL_AND_INVITE_ID_ARE_REQUIRED,
    ERROR_EMAIL_IS_REQUIRED,
    ERROR_INVALID_EMAIL_OR_PASSWORD,
    ERROR_INVALID_INVITE_ID,
    ERROR_INVALID_PASSWORD_RESET_ID,
    ERROR_INVITE_HAS_EXPIRED,
    ERROR_NO_BOTS_ALLOWED,
    ERROR_PASSWORD_IS_REQUIRED,
    ERROR_PASSWORD_PWNED,
    ERROR_PASSWORD_RESET_LINK_HAS_EXPIRED,
    ERROR_PASSWORD_NO_SPACES,
    ERROR_PASSWORD_TOO_SHORT,
    ERROR_PASSWORD_TOO_WEAK
)
from piste.app.main import app
from flask import request
from flask_jwt_extended import create_access_token, get_jwt_identity
from pystache import render
from ujson import dumps
from zxcvbn import zxcvbn

log = getLogger(__name__)


def get_email_template_body_html():
    filename = path.join(path.dirname(__file__), 'email-template.html')
    with open(filename, encoding='utf-8') as f:
        return f.read()


EMAIL_TEMPLATE_BODY_HTML = get_email_template_body_html()


class User:
    default_response = {'status': 'ok'}

    @staticmethod
    def _error_response(error):
        return {'error': error}

    @staticmethod
    def _get(user_id):
        """
        IMPORTANT: This function must return the entire user record - only for use by update
        """
        return app.db.one('select * from public.user where id=%(user_id)s', {'user_id': user_id})

    @staticmethod
    def _get_by_email(email):
        return app.db.one("select * from public.user where data->>'email'=%(email)s", {'email': email})

    @staticmethod
    def _post_recaptcha_response(recaptcha_response):
        return app.external.post(
            'https://www.google.com/recaptcha/api/siteverify',
            params={
                'secret': app.env('RECAPTCHA_SECRET_KEY'),
                'response': recaptcha_response,
                'remoteip': request.remote_addr
            }).json()

    @staticmethod
    def _send_email(subject, body_text, base_parameters, email, action_id):
        host = app.env('EMAIL_HOST', '')
        origin = 'https://{}'.format(host) if app.production else 'http://localhost:3000'
        project_name = app.env('EMAIL_PROJECT_NAME', 'Project Gnar')
        source = '{} <{}>'.format(project_name, app.env('EMAIL_SENDER'))
        token = urlsafe_b64encode('{}/{}'.format(email, action_id).encode('utf-8')).decode('utf-8')
        parameters = {**base_parameters,
                      'host': host,
                      'origin': origin,
                      'project_name': project_name,
                      'logo_url': app.env('EMAIL_LOGO_URL'),
                      'token': token}
        app.get_ses_client().send_email(
            Destination={'ToAddresses': [email]},
            Message={
                'Body': {
                    'Html': {
                        'Charset': EMAIL_CHARSET,
                        'Data': render(EMAIL_TEMPLATE_BODY_HTML, parameters)
                    },
                    'Text': {
                        'Charset': EMAIL_CHARSET,
                        'Data': body_text.format(origin, token)
                    }
                },
                'Subject': {
                    'Charset': EMAIL_CHARSET,
                    'Data': subject
                }
            },
            Source=source)
        return token

    @staticmethod
    def _update(id, data):
        app.db.run('update public.user set data=%(data)s where id=%(id)s', {'id': id, 'data': dumps(data)})

    @staticmethod
    def _whitelist_user(id, user):
        whitelist_user = {k: v for k, v in user.items() if k in DATA_READ_WHITELIST}
        whitelist_user['user_id'] = id
        return whitelist_user

    @classmethod
    def activate(cls, token):
        [email, invite_id] = urlsafe_b64decode(token).decode('utf-8').split('/')
        if not email or not invite_id:
            return cls._error_response(ERROR_EMAIL_AND_INVITE_ID_ARE_REQUIRED)
        user = cls._get_by_email(email)
        if not user:
            return cls._error_response(ERROR_USER_DOES_NOT_EXIST)
        if user.data.get('password'):
            return cls._error_response(ERROR_USER_HAS_ALREADY_BEEN_ACTIVATED)
        invite = user.data.get('invite', {})
        if invite.get('id') != invite_id:
            return cls._error_response(ERROR_INVALID_INVITE_ID)
        invite_time = datetime.fromtimestamp(invite.get('time'))
        if datetime.utcnow() - invite_time > timedelta(minutes=60):
            return cls._error_response(ERROR_INVITE_HAS_EXPIRED)
        return {'user_id': user.id, 'access_token': create_access_token(email)}

    @classmethod
    def get(cls, email):
        if not email:
            return cls._error_response(ERROR_EMAIL_IS_REQUIRED)
        user = cls._get_by_email(email)
        return cls._whitelist_user(user.id, user.data) if user else {}

    @classmethod
    def login(cls, email, password, recaptcha_response):
        recaptcha_verification = cls._post_recaptcha_response(recaptcha_response)
        if not recaptcha_verification.get('success'):
            return cls._error_response(ERROR_NO_BOTS_ALLOWED), None
        record = cls._get_by_email(email)
        if record and app.check_password_hash(record.data['password'], password):
            if record.data.get('locked'):
                return cls._error_response(ERROR_ACCOUNT_LOCKED), None
            else:
                return cls._whitelist_user(record.id, record.data), create_access_token(email)
        else:
            return cls._error_response(ERROR_INVALID_EMAIL_OR_PASSWORD), None

    @classmethod
    def reset_password(cls, action_id, email, password):
        if not action_id or not email or not password:
            return cls._error_response(ERROR_ACTION_ID_EMAIL_AND_NEW_PASSWORD_ARE_REQUIRED)
        user = cls._get_by_email(email)
        if not user:
            return cls._error_response(ERROR_USER_DOES_NOT_EXIST)
        old_password = user.data.get('password')
        if not old_password:
            return cls._error_response(ERROR_USER_HAS_NOT_BEEN_ACTIVATED)
        password_reset = user.data.get('passwordReset', {})
        if password_reset.get('id') != action_id:
            return cls._error_response(ERROR_INVALID_PASSWORD_RESET_ID)
        now = datetime.utcnow()
        reset_password_time = datetime.fromtimestamp(password_reset.get('time'))
        if now - reset_password_time > timedelta(minutes=10):
            return cls._error_response(ERROR_PASSWORD_RESET_LINK_HAS_EXPIRED)
        data = {**user.data, 'password': app.generate_password_hash(password)}
        data.setdefault('passwordResets', []).insert(0, {'password': old_password, 'dttm': now.timestamp()})
        data.pop('passwordReset')
        cls._update(user.id, data)
        return cls.default_response

    @classmethod
    def score_password(cls, password):
        if not password:
            return cls._error_response(ERROR_PASSWORD_IS_REQUIRED)
        results = zxcvbn(password)
        return {k: results[k] for k in ('score', 'feedback')}

    @classmethod
    def send_reset_password_email(cls, email, recaptcha_response):
        if not email:
            return cls._error_response(ERROR_EMAIL_IS_REQUIRED)
        recaptcha_verification = cls._post_recaptcha_response(recaptcha_response)
        if not recaptcha_verification.get('success'):
            return cls._error_response(ERROR_NO_BOTS_ALLOWED)
        user = cls._get_by_email(email)
        if not user:
            # We send a normal response because it is a security risk
            # to provide a response that indicates a user does not exist.
            app.db.run('insert into public."badPasswordResetRequests" (email, ip) values (%(email)s, %(ip)s)',
                       {'email': email, 'ip': request.remote_addr})
            return cls.default_response
        if not user.data.get('password'):
            # We'll send an activation email instad of a password reset email.
            # We return a normal response because it is a security risk to do otherwise.
            cls.signup(email, existing_user=user)
            return cls.default_response
        action_id = str(uuid4())
        data = {**user.data, 'passwordReset': {'id': action_id, 'time': datetime.utcnow().timestamp()}}
        cls._update(user.id, data)
        cls._send_email(EMAIL_RESET_PASSWORD_SUBJECT, EMAIL_RESET_PASSWORD_BODY_TEXT, EMAIL_RESET_PASSWORD_PARAMETERS,
                        email, action_id)
        return cls.default_response

    @classmethod
    def signup(cls, email, recaptcha_response=None, existing_user=None):
        if not email:
            return cls._error_response(ERROR_EMAIL_IS_REQUIRED)
        if not existing_user:
            recaptcha_verification = cls._post_recaptcha_response(recaptcha_response)
            if not recaptcha_verification.get('success'):
                return cls._error_response(ERROR_NO_BOTS_ALLOWED)
        existing_user = existing_user or cls._get_by_email(email)
        if existing_user and existing_user.data.get('password'):
            return cls._error_response(ERROR_USER_ALREADY_EXISTS)
        action_id = str(uuid4())
        data = {'email': email, 'invite': {'id': action_id, 'time': datetime.utcnow().timestamp()}}
        if existing_user:
            user_id = existing_user.id
            cls._update(existing_user.id, data)
        else:
            user_id = str(uuid4())
            app.db.run('insert into public.user (id, data) values (%(id)s, %(data)s)',
                       {'id': user_id, 'data': dumps(data)})
        cls._send_email(EMAIL_SIGNUP_SUBJECT, EMAIL_SIGNUP_BODY_TEXT, EMAIL_SIGNUP_PARAMETERS, email, action_id)
        return cls.default_response

    @classmethod
    def update(cls, user_id, data):
        record = cls._get(user_id)
        if record and getattr(record, 'data', {}).get('email') == get_jwt_identity():
            whitelist_data = {k: v for k, v in data.items() if k in DATA_UPDATE_WHITELIST}
            if 'password' in data:
                password = data['password']
                if len(password) < 8:
                    return cls._error_response(ERROR_PASSWORD_TOO_SHORT)
                if ' ' in password:
                    return cls._error_response(ERROR_PASSWORD_NO_SPACES)
                if cls.score_password(password)['score'] < 4:
                    return cls._error_response(ERROR_PASSWORD_TOO_WEAK)
                digest = hashlib.new('sha1', password.encode('utf8')).hexdigest()
                matches = app.external.get('https://api.pwnedpasswords.com/range/{}'.format(digest[:5])).text
                if re.search(r'{}:'.format(digest[5:]), matches, re.I):
                    return cls._error_response(ERROR_PASSWORD_PWNED)
                whitelist_data['password'] = app.generate_password_hash(password)
            data = {**record.data, **whitelist_data}
            cls._update(user_id, data)
            return cls._whitelist_user(user_id, data)
        else:
            return {}
