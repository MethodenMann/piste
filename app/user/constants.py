DATA_UPDATE_WHITELIST = set(['firstName', 'lastName', 'address1', 'address2', 'city', 'state', 'postalCode'])
DATA_READ_WHITELIST = DATA_UPDATE_WHITELIST | set([])  # [Project Gnar]: Add any read-only fields to this set

EMAIL_CHARSET = 'UTF-8'
EMAIL_RESET_PASSWORD_BODY_TEXT = 'Forgot your password? To reset your password, please go to {}/reset-password/{}'
EMAIL_RESET_PASSWORD_PARAMETERS = {
    'action': 'reset-password',
    'title': 'Forgot your password?',
    'text1': 'We heard you need a password reset. Click the button below to enter our secure password reset site.',
    'text2': 'If you did not request a password reset, you can safely ignore this email and continue using Gnar '
             'with your current password.',
    'text2_display': 'block',
    'button_text_placeholder': 'Reset my password',
    'initiated_by': 'requested a password reset'
}
EMAIL_RESET_PASSWORD_SUBJECT = 'Reset your password'
EMAIL_SIGNUP_BODY_TEXT = 'Thanks for signing up! To activate your account, please go to {}/activate/{}'
EMAIL_SIGNUP_PARAMETERS = {
    'action': 'activate',
    'title': 'Hey, thanks for signing up!',
    'text1': 'Confirm your email to get started.',
    'text2': '',
    'text2_display': 'none',
    'button_text_placeholder': 'Confirm my email address',
    'initiated_by': 'signed up'
}
EMAIL_SIGNUP_SUBJECT = 'Confirm your email address'

ERROR_ACCOUNT_LOCKED = 'Your account is locked. Please contact Project Gnar for assistance.'
ERROR_ACTION_ID_EMAIL_AND_NEW_PASSWORD_ARE_REQUIRED = 'Action id, email, and new password are required.'
ERROR_USER_ALREADY_EXISTS = 'User already exists.'
ERROR_USER_DOES_NOT_EXIST = 'User does not exist.'
ERROR_USER_HAS_ALREADY_BEEN_ACTIVATED = 'This account has already been activated.'
ERROR_USER_HAS_NOT_BEEN_ACTIVATED = 'User has not been activated.'
ERROR_EMAIL_AND_INVITE_ID_ARE_REQUIRED = 'Email and invite id are required.'
ERROR_EMAIL_IS_REQUIRED = 'Email is required.'
ERROR_INVALID_EMAIL_OR_PASSWORD = 'Invalid email or password.'
ERROR_INVALID_INVITE_ID = 'Invalid invite id.'
ERROR_INVALID_PASSWORD_RESET_ID = 'Invalid pasword reset id.'
ERROR_INVITE_HAS_EXPIRED = 'Invite has expired.'
ERROR_NO_BOTS_ALLOWED = 'Sorry, no bots allowed!'
ERROR_PASSWORD_IS_REQUIRED = 'Password is required.'
ERROR_PASSWORD_PWNED = 'Password has been pwned.'
ERROR_PASSWORD_RESET_LINK_HAS_EXPIRED = 'Password reset link has expired.'
ERROR_PASSWORD_NO_SPACES = 'Password cannot contain spaces.'
ERROR_PASSWORD_TOO_SHORT = 'Password must be at least 8 characters.'
ERROR_PASSWORD_TOO_WEAK = 'Password score is too low.'
