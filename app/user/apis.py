from flask import make_response
from logging import getLogger

from flask import Blueprint, jsonify, request
from flask_jwt_extended import jwt_required

from piste.app.user.services import User

api_name = 'user'
url_prefix = '/{}'.format(api_name)

# [Project Gnar]: This variable must be `blueprint` for compatibility with `gnar-gear`
blueprint = Blueprint(api_name, __name__, url_prefix=url_prefix)

log = getLogger(__name__)


@blueprint.route('/activate', methods=['POST'])
def user_activate():
    data = request.get_json(force=True)
    token = data.get('token')
    response = User.activate(token)
    return jsonify(response)


@blueprint.route('/get', methods=['POST'])
@jwt_required
def user_get():
    data = request.get_json(force=True)
    response = User.get(data.get('email'))
    return jsonify(response)


@blueprint.route('/login', methods=['POST'])
def user_login():
    data = request.get_json(force=True)
    email = data.get('email')
    password = data.get('password')
    recaptcha_response = data.get('reCaptchaResponse')
    response, token = User.login(email, password, recaptcha_response)
    response = make_response(jsonify(response))
    if token:
        response.headers['Authorization'] = 'Bearer {}'.format(token)
    return response


@blueprint.route('/reset-password', methods=['POST'])
def user_reset_password():
    data = request.get_json(force=True)
    action_id = data.get('actionId')
    email = data.get('email')
    password = data.get('password')
    response = User.reset_password(action_id, email, password)
    return jsonify(response)


@blueprint.route('/score-password', methods=['POST'])
def score_password():
    data = request.get_json(force=True)
    password = data.get('password')
    response = User.score_password(password)
    return jsonify(response)


@blueprint.route('/send-reset-password-email', methods=['POST'])
def user_send_reset_password_email():
    data = request.get_json(force=True)
    email = data.get('email')
    recaptcha_response = data.get('reCaptchaResponse')
    response = User.send_reset_password_email(email, recaptcha_response)
    return jsonify(response)


@blueprint.route('/signup', methods=['POST'])
def user_signup():
    data = request.get_json(force=True)
    email = data.get('email')
    recaptcha_response = data.get('reCaptchaResponse')
    response = User.signup(email, recaptcha_response)
    return jsonify(response)


@blueprint.route('/<user_id>', methods=['PUT'])
@jwt_required
def user_update(user_id):
    response = User.update(user_id, request.get_json(force=True))
    return jsonify(response)
